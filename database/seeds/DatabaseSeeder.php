<?php
use App\User;
use App\Article;
use Illuminate\Support\Facades\DB;
use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS = 0');

        User::truncate();
        Article::truncate();

        $usersQuantity = 10;
        $articlesQuantity = 30;

        factory(User::class, $usersQuantity)->create();
        factory(Article::class, $articlesQuantity)->create();
    }
}
