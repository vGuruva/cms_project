let mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel application. By default, we are compiling the Sass
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/assets/js/app.js', 'public/js')
	.sass('resources/assets/sass/app.scss', 'public/css')
	.copyDirectory('node_modules/font-awesome/fonts', 'public/fonts')
   	.styles([
	   	'node_modules/font-awesome/css/font-awesome.css',
	    'resources/assets/css/argon.css',
	     'resources/assets/css/custom.css'
], 'public/css/styles.css')

   	.scripts([
    '/node_modules/headroom.js/dist/headroom.min.js',
    'resources/assets/js/argon.min.js'
], 'public/js/scripts.js');
