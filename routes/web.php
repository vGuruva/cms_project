<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

	Route::get('/', function () {
	    return view('welcome');
	});

	Auth::routes();

	Route::get('/articles', 'ArticleController@index')->name('articles');

	Route::get('/article/add', 'ArticleController@create')->name('article_add');

	Route::post('/article/store', 'ArticleController@store')->name('article_store');

	Route::get('/article/edit/{id}', 'ArticleController@edit')->name('article_edit');

	Route::post('/article/update/{id}', 'ArticleController@update')->name('article_update');

	Route::post('/article/delete/{id}', 'ArticleController@destroy')->name('article_destroy');

	Route::get('/article/show/{id}', 'ArticleController@show')->name('article_show');



