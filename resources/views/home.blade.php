@extends('layouts.custom')

@section('content')
<section class="section section-components pb-0" id="section-components">
  <div class="container">
    <div class="row justify-content-center shift-top">
      <div class="col-lg-12">
        <h2 class="mb-5">
          <span>Articles List</span>
        </h2>
        <table class="table">
          <thead class="thead-dark">
            <tr>
              <th scope="col">#</th>
              <th scope="col">Name</th>
              <th scope="col">Description</th>
              <th scope="col">Edit</th>
              <th scope="col">Delete</th>
            </tr>
          </thead>
          <tbody>
              @foreach($articles as $article)
            <tr>
              <th scope="row">{{ $article->id }}</th>
                <td>{{ $article->name }}</td>
                <td>{{ $article->description }}</td>
                <td>        
                  <a href="/article/edit/{{ $article->id }}"><button type="button" class="btn btn-default btn-sm">
                      <i class="fa fa-pencil" aria-hidden="true"></i>
                  </button></a>
              </td>
              <td>              
                <a href="/article/delete/{{ $article->id }}"><button type="submit" class="btn btn-default btn-sm">
                      <i class="fa fa-trash" aria-hidden="true"></i>
                </button>
              </a>
              </td>
            </tr>
            @endforeach
          </tbody>
        </table>
      </div>
    </div>
  </div>
</section>
@endsection