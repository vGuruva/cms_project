<?php $__env->startSection('content'); ?>
<div class="position-relative">
  <section class="section-hero section-shaped my-0">
    <div class="shape shape-style-1 shape-primary">
      <span class="span-150"></span>
      <span class="span-50"></span>
      <span class="span-50"></span>
      <span class="span-75"></span>
      <span class="span-100"></span>
      <span class="span-75"></span>
      <span class="span-50"></span>
      <span class="span-100"></span>
      <span class="span-50"></span>
      <span class="span-100"></span>
    </div>
    <div class="container shape-container d-flex align-items-center">
      <div class="col px-0">
        <div class="row justify-content-center align-items-center">
          <div class="col-lg-7 text-center pt-lg">
            <p class="lead text-white mt-4 mb-5">Welcome, thanks for chosing us, would you like to create an article? Go on give it a go.</p>
            <div class="btn-wrapper">
              <a href="<?php echo e(url('/article/add' )); ?>" class="btn btn-info btn-icon mb-3 mb-sm-0" data-toggle="scroll">
                <span class="btn-inner--icon"><i class="fa fa-code"></i></span>
                <span class="btn-inner--text">Create and article</span>
              </a>
              <a href="<?php echo e(url('articles')); ?>" class="btn btn-white btn-icon mb-3 mb-sm-0">
                <span class="btn-inner--icon"><i class="ni ni-cloud-download-95"></i></span>
                <span class="btn-inner--text">View articles list</span>
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.custom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>