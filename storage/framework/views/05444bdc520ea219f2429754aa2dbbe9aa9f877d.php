<?php $__env->startSection('content'); ?>
<section class="section section-shaped section-lg my-0">
  <div class="container pt-lg-md">
    <div class="row justify-content-center">
      <div class="col-lg-9">
      	<h2 class="mb-5">
          <span>Adding Category</span>
        </h2>
        <div class="card bg-secondary shadow border-0">
          <div class="card-body px-lg-5 py-lg-5">
            <form method="POST" action="<?php echo e(route('category_store')); ?>" aria-label="<?php echo e(__('Login')); ?>">
                <?php echo csrf_field(); ?>
              <div class="form-group mb-3">
              	<label>Name</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-email-83"></i></span>
                  </div>
                  <input id="name" type="text" class="form-control" name="name" placeholder="Name" required autofocus>
                </div>
              </div>
              <div class="form-group">
              	<label>Description</label>
                <div class="input-group input-group-alternative">
                  <div class="input-group-prepend">
                    <span class="input-group-text"><i class="ni ni-lock-circle-open"></i></span>
                  </div>
                  <input id="description" type="text" class="form-control" name="description" placeholder="Description" required autofocus>
                </div>
              </div>
              <div >
                <button type="submit" type="button" class="btn btn-success my-4"><?php echo e(__('Save')); ?></button>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.custom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>