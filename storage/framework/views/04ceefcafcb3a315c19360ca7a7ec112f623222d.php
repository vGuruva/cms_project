<?php $__env->startSection('content'); ?>
<section class="section-profile-cover section-shaped my-0">
  <div class="shape shape-style-1 shape-primary shape-skew alpha-4">
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
    <span></span>
  </div>
</section>
<section class="section section-skew">
  <div class="container">
    <div class="card card-profile shadow mt--300">
      <div class="px-4">
        <div class="row justify-content-center">
          <div class="col-lg-3 order-lg-2">
            <div class="card-profile-image">
              <a href="#">
                <img src="/storage/feature_images/<?php echo e($article->feature_image); ?>" class="article-img">
              </a>
            </div>
          </div>
          <div class="col-lg-4 order-lg-3 text-lg-right align-self-lg-center">
            <div class="card-profile-actions py-4 mt-lg-0">
          <?php if(!Auth::guest()): ?>
            <?php if(Auth::user()->id == $article->user_id): ?>
              <a href="/article/edit/<?php echo e($article->id); ?>" class="btn btn-sm btn-info mr-4">Edit</a>
              <?php echo Form::open(['action' => ['ArticleController@destroy', $article->id], 'method' => 'POST', 'class' => 'pull-right']); ?>

              <a href="#" type="submit" class="btn btn-sm btn-default float-right">Delete</a>
              <?php echo Form::close(); ?>

            <?php endif; ?>
          <?php endif; ?>
            </div>
          </div>
          <div class="col-lg-4 order-lg-1">
          </div>
        </div>
        <div class="text-center mt-5">
          <h3><?php echo e($article->name); ?></h3>
          <div class="h6 font-weight-300"><i class="ni location_pin mr-2"></i> By <?php echo e($article->user->name); ?></div>
          <div class="h6 font-weight-300"><i class="ni location_pin mr-2"></i><?php echo e($article->created_at); ?></div>
        </div>
        <div class="mt-5 py-5 border-top text-center">
          <div class="row justify-content-center">
            <div class="col-lg-9">
              <p><?php echo $article->description; ?></p>
              <a href="<?php echo e(url('articles')); ?>">Back to articles list</a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.custom', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>